import React from 'react';

export default class Contador extends React.Component{
    constructor(){
        super();
        this.state = {
          contador: 0,
        };
    }

    incrementar(){
        //setState é o método necessário para alterar um estado criado
        this.setState({
            contador: this.state.contador + 1,
        });
    }

    decrementar(){
        this.setState({
            contador: this.state.contador - 1,
        });
    }

    render(){
        return (
            <div>
                <h1>Contador</h1>
                <h2>{this.state.contador}</h2>
                <div>
                    <button onClick={this.decrementar.bind(this)}>-</button>
                    <button onClick={this.incrementar.bind(this)}>+</button>
                </div>
            </div>
        )
    }
}
