import React from 'react'

//import Contador from './components/contador';

function header(){
    return(
        <h1>Wolfbook</h1>
    )
}

export default header;

function formatName(user) {
    return user.firstName + ' ' + user.lastName;
}

const user = {
    firstName: 'Harper',
    lastName: 'Perez'
};

const element = (
    <div>
        <h1>Hello!</h1>
        <h2>Good to see you here.</h2>
    </div>
);

//const element = <img src={user.avatarUrl}></img>;

function getGreeting(user) {
    if (user) {
        return <h1>Hello, {formatName(user)}!</h1>;
    }
    return <h1>Hello, Stranger.</h1>;
}

const logo = <img id="logo" src="./lobo.jpg" alt="logo" />;

//Função relógio
function tick() {
    const element = (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {new Date().toLocaleTimeString()}.</h2>
      </div>
    );
    ReactDOM.render(element, document.getElementById('root'));
  }
  
  setInterval(tick, 1000);

//Função App
function App() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
}
  

//Propers
function Welcome(props) {
    return <h1>Hello, {props.name}</h1>;
}
  
function App() {
    return (
        <div>
        <Welcome name="Sara" />
        <Welcome name="Cahal" />
        <Welcome name="Edite" />
        </div>
    );
}

//import PlacarContainer from './components/PlacarContainer';

const dados = {
  partida: {
    estadio: "Maracanã/RJ",
    data: "23/08/2020",
    horario: "19h",
  },
  casa: {
    nome: "Vasco",
  },
  visitante: {
    nome: "Flamengo",
  }
};

export default class App extends React.Component{
  render(){
    return (
      <PlacarContainer {...dados}/>
    );
  }
}