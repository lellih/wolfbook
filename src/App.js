import React from 'react';
//import logo from './logo.svg';
import './App.css';

class App extends React.Component{
  render(){
    return (
      <div className="header">
        <div>
          <h1>Wolfbook</h1>
        </div>

        <nav className="navbar">
          <a href="#">Auline</a>
          <a href="#">Adicionar Lobos</a>
          <a href="#">Sua Alcateia</a>
          <a className="exit" href="#"><i class="fas fa-sign-out-alt"></i></a>
        </nav>

      </div>
    );
  }
}

export default App;